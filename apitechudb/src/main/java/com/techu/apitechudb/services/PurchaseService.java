package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService {
    @Autowired
    PurchaseRepository purchaseRepository;

    public List<PurchaseModel> findAll(){
        System.out.println("findAlL en PurchaseModel ");

        return  this.purchaseRepository.findAll();
    }

    public Optional<PurchaseModel> findById(String id){
        System.out.println("findById en PurchaseModel ");

        return this.purchaseRepository.findById(id);
    }

    public PurchaseModel add(PurchaseModel purchase){
        System.out.println("add en PurchaseModel");

        return this.purchaseRepository.save(purchase);
    }

    public PurchaseModel update(PurchaseModel purchase){
        System.out.println("updateById en PurchaseModel");

        return this.purchaseRepository.save(purchase);
    }
    public boolean delete(String id){
        System.out.println("delete en PurchaseModel");
        boolean result = false;

        if(findById(id).isPresent()){
            System.out.println("Purchase para eliminar encontrado, eliminando");
            purchaseRepository.deleteById(id);
            result = true;
        }
        return result;
    }
}
