package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        System.out.println("findAlL en UserService ");

        return userRepository.findAll();
    }
    public List<UserModel> findbyAge(String orden) {
        System.out.println("findAlL en UserService ");
        Sort.Direction sort = Sort.Direction.ASC;

        if(orden.equals("DESC")){
             sort = Sort.Direction.DESC;
        }
        return userRepository.findAll(Sort.by(sort, "age"));
    }


    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserService ");

        return userRepository.findById(id);
    }

    public UserModel add(UserModel user) {
        System.out.println("add en UserService");

        return userRepository.save(user);
    }

    public UserModel update(UserModel user) {
        System.out.println("update en UserService");

        return userRepository.save(user);
    }

    public boolean delete(String id) {
        System.out.println("delete en UserService");
        boolean result = false;

        if (findById(id).isPresent()) {
            System.out.println("User para eliminar encontrado, eliminando");
            userRepository.deleteById(id);
            result = true;
        }
        return result;
    }

}
