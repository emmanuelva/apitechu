package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins="*", methods = {RequestMethod.GET, RequestMethod.DELETE})
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(required = false) String orden){
        System.out.println("getUsers ");
        if(orden==null || orden.isEmpty()){
            orden="ASC";
        }
        return new ResponseEntity<>(
                userService.findbyAge(orden),
                HttpStatus.OK
        );
    }
    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUsersById(@PathVariable String id){
        System.out.println("getUsersById");
        System.out.println("El id  abuscar es: " + id);

        Optional<UserModel> result = userService.findById(id);


        return new ResponseEntity<>(
                result.isPresent() ? result:  "No hay users con ese ID",
                result.isPresent() ? HttpStatus.OK:  HttpStatus.NOT_FOUND);
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del user a crear es: " + user.getId());
        System.out.println("El name del user a crear es: " + user.getName());
        System.out.println("La edad del user a crear es: " + user.getAge());

        return new ResponseEntity<>(
                userService.add(user), HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("updateUser");
        System.out.println("La id del user de URL  es: " + id);
        System.out.println("El name del user a actualizar es: " + user.getName());
        System.out.println("La edad del user a actualizar es: " + user.getAge());

        Optional<UserModel>userToUpdate= userService.findById(id);

        if(userToUpdate.isPresent()){
            System.out.println("Producto para actualizar encontrado, actualizando");
            user.setId(id);

            userService.update(user);
        }

        return new ResponseEntity<>(
                user,
                userToUpdate.isPresent()?HttpStatus.OK:HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("La id del user de URL a borrar es: " + id);

        boolean deleteUser = userService.delete(id);

        return new ResponseEntity<>(
                deleteUser? "Se ha borrado  id: " + id: "No se ha borrado el ID",
                deleteUser? HttpStatus.OK:HttpStatus.NOT_FOUND
        );

    }

}
