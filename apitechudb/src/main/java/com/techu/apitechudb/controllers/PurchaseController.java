package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins="*", methods = {RequestMethod.GET, RequestMethod.DELETE})
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases(){
        System.out.println("getPurchases ");

        return new ResponseEntity<>(
                purchaseService.findAll(),
                HttpStatus.OK
        );
    }
    @GetMapping("/purchases/{id}")
    public ResponseEntity<Object> getPurchaseById(@PathVariable String id){
        System.out.println("etPurchaseById");
        System.out.println("La id del purchase a buscar es: " + id);

        Optional<PurchaseModel> result = purchaseService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get():  "Purchase no encontrado",
                result.isPresent() ? HttpStatus.OK:  HttpStatus.NOT_FOUND);
    }

    @PostMapping("/purchases")
    public ResponseEntity<Object> addPurchase(@RequestBody PurchaseModel purchase){
        System.out.println("addPurchase");
        System.out.println("La id del purchase a crear es: " + purchase.getId());
        System.out.println("La descripcion del purchase a crear es: " + purchase.getUserId());
        System.out.println("El precio del purchase a crear es: " + purchase.getAmount());
        System.out.println("Los  purchaseItems del purchase a crear es: " + purchase.getPurchaseItems());

        Optional<UserModel> usertoPurchase= userService.findById(purchase.getUserId());

        if(usertoPurchase != null && !usertoPurchase.isEmpty()){
            System.out.println("Id del usuario  encontrado en base de datos, añadidendo a la compra");

            purchaseService.add(purchase);
        }

        Optional<ProductModel> producToPurchase= productService.findById("");

        return new ResponseEntity<>(
                usertoPurchase.isPresent()? purchase:"No se ha creado la compra, ID de usuario no existe en BBDD", usertoPurchase.isPresent()?HttpStatus.OK:HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/purchases/{id}")
    public ResponseEntity<PurchaseModel> updatePurchase(@RequestBody PurchaseModel purchase, @PathVariable String id){
        System.out.println("updatePurchase");
        System.out.println("La id del purchase de URL  es: " + id);
        System.out.println("La id del purchase a updatear es: " + purchase.getId());
        System.out.println("La descripcion del purchase a updatear es: " + purchase.getUserId());
        System.out.println("El precio del purchase a updatear es: " + purchase.getAmount());
        System.out.println("Los  purchaseItems del purchase a updatear es: " + purchase.getPurchaseItems());

        Optional<PurchaseModel> purchasetToUpdate= purchaseService.findById(id);

        if(purchasetToUpdate.isPresent()){
            System.out.println("Purchase para actualizar encontrado, actualizando");

            purchaseService.update(purchase);
        }

        return new ResponseEntity<>(
                purchase,
                purchasetToUpdate.isPresent()?HttpStatus.OK:HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/purchases/{id}")
    public ResponseEntity<String> deletePurchase(@PathVariable String id){
        System.out.println("deletePurchase");
        System.out.println("La id del Purchase de URL a borrar es: " + id);

        boolean deleteProduct = purchaseService.delete(id);

        return new ResponseEntity<>(
                deleteProduct? "Se ha borrado  id: " + id: "No se ha borrado el ID",
                deleteProduct? HttpStatus.OK:HttpStatus.NOT_FOUND
        );

    }

}
