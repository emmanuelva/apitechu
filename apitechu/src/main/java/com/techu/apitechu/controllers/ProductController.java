package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseURL ="/apitechu/v1";

    @GetMapping(APIBaseURL + "/products")
    public ArrayList<ProductModel> getProducts(){
      System.out.println("getProducts");

      return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseURL + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProdyctById");
        System.out.println("La id del producto a buscar es: " + id);

        ProductModel result = new ProductModel();
        for(ProductModel product: ApitechuApplication.productModels){
            if(product.getId().equals(id)){
                result=product;
            }
        }
        return result;
    }
    @PostMapping(APIBaseURL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La id del nuevo producto a crear es: " + newProduct.getId());
        System.out.println("La descripcion del nuevo producto a crear es: " + newProduct.getDesc());
        System.out.println("El precio del nuevo producto a crear es: " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseURL + "/products/{id}")
    public ProductModel updateProduct(
            @RequestBody ProductModel product, @PathVariable String id)
    {
        System.out.println("updateProduct");
        System.out.println("La id del  producto a actualizar en parametro URL es: " + product.getId());
        System.out.println("La id del  producto a actualizar es: " + product.getId());
        System.out.println("La descripcion del  producto a actualizar es: " + product.getDesc());
        System.out.println("El precio del  producto a actualizar es: " + product.getPrice());

        for(ProductModel productInList: ApitechuApplication.productModels){
            if(productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;
    }
    @PatchMapping(APIBaseURL + "/products/{id}")
    public ProductModel patchProduct(
            @RequestBody ProductModel productPatch, @PathVariable String id)
    {
        productPatch.setId(id);
        System.out.println("updatePatchProduct");
        System.out.println("La id del  producto a actualizar parcialmente en parametro URL es: " + productPatch.getId());
        System.out.println("La descripcion del  producto a actualizar  es: " + productPatch.getDesc());
        System.out.println("El precio del  producto a actualizar  es: " + productPatch.getPrice());


        for(ProductModel productInList: ApitechuApplication.productModels){
            if(productInList.getId().equals(id)){
                if(productPatch.getDesc() != null && !productPatch.getDesc().isEmpty()  && !productInList.getDesc().equals(productPatch.getDesc())){
                    productInList.setDesc(productPatch.getDesc());
                    System.out.println("Se ha actualizado productPatch.getDesc() " + productPatch.getDesc());
                }
                if(productPatch.getPrice() >= 0 &&  productInList.getPrice() != productPatch.getPrice()){
                    productInList.setPrice(productPatch.getPrice());
                    System.out.println("Se ha actualizado productPatch.getPrice() " + productInList.getPrice());
                }
            }else{
                System.err.println("No se ha actualizado productPatch.getId() " + productPatch.getId() +" no existe este id");
                return null;
            }
        }

        return  productPatch;
    }

    @DeleteMapping(APIBaseURL + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println("deletePatchProduct");
        System.out.println("La id del  producto a borrar es: " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;

        for(ProductModel productInList: ApitechuApplication.productModels){
            if(productInList.getId().equals(id)){
                System.out.println("Producto para borrar encontrado");
                result= productInList;
                foundCompany=true;
            }
        }
        if(foundCompany){
            System.out.println("Borrando producto");
            ApitechuApplication.productModels.remove(result);
        }
        return result;
    }
}
